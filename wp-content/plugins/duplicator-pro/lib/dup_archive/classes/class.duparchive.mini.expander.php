<?php

//---------- DUPARCHIVE MINI EXPANDER: The contents of this file will be injected into the installer bootlog at build time ------------------------

class DupArchiveMiniItemHeaderType
{
    const None      = 0;
    const File      = 1;
    const Directory = 2;
    const Glob      = 3;
}

class DupArchiveMiniFileHeader
{
    public $fileSize;
    public $mtime;
    public $permissions;
    public $md5;
    public $relativePathLength;
    public $relativePath;

    const MaxHeaderSize                = 8192;
    const MaxStandardHeaderFieldLength = 128;

    #F#{$file_size}#{$mtime}#{$file_perms}#{$md5}#{$relative_filepath_length}#{$relative_filepath}!

    static function readFromArchive($archiveHandle)
    {
        // Will skip past the contents of the file if relative filepath foesnt match filter

        $instance = new DupArchiveMiniFileHeader();

        //   $headerString = "?F#{$this->fileSize}#{$this->mtime}#{$this->filePermissions}#{$this->md5}#{$this->relativeFilepathLength}#{$this->relativeFilepath}!";

        $instance->fileSize           = self::readStandardHeaderField($archiveHandle);
        $instance->mtime              = self::readStandardHeaderField($archiveHandle);
        $instance->permissions        = self::readStandardHeaderField($archiveHandle);
        $instance->md5                = self::readStandardHeaderField($archiveHandle);
        $instance->relativePathLength = self::readStandardHeaderField($archiveHandle);
        $instance->relativePath       = fread($archiveHandle, $instance->relativePathLength);

        // Skip the #F!
        fread($archiveHandle, 3);

        return $instance;
    }

    private static function readStandardHeaderField($archiveHandle)
    {
        $s = stream_get_line($archiveHandle, self::MaxStandardHeaderFieldLength, '#');

        if($s === false) {
            throw new Exception("Coudln't read standard header field in file header.");
        }

        return $s;
    }
}

class DupArchiveMiniDirectoryHeader
{
    public $mtime;
    public $permissions;
    public $relativePathLength;
    public $relativePath;

    const MaxHeaderSize                = 8192;
    const MaxStandardHeaderFieldLength = 128;

    static function readFromArchive($archiveHandle, $skipMarker = false)
    {
        $instance = new DupArchiveMiniDirectoryHeader();

        if (!$skipMarker) {
            $marker = fgets($archiveHandle, 4);

            if ($marker === false) {
                if (feof($archiveHandle)) {
                    return false;
                } else {
                    new Exception('Error reading directory header');
                }
            }

            if ($marker != '?D#') {
                throw new Exception("Invalid directory header marker found [{$marker}] : location ".ftell($archiveHandle));
            }
        }

        // ?D#{mtime}#{permissions}#{$this->relativePathLength}#{$relativePath}#D!";

        $instance->mtime              = self::readStandardHeaderField($archiveHandle);
        $instance->permissions        = self::readStandardHeaderField($archiveHandle);
        $instance->relativePathLength = self::readStandardHeaderField($archiveHandle);
        $instance->relativePath       = fread($archiveHandle, $instance->relativePathLength);

        // Skip the #D!
        fread($archiveHandle, 3);

        return $instance;
    }

    private static function readStandardHeaderField($archiveHandle)
    {
        $s = stream_get_line($archiveHandle, self::MaxStandardHeaderFieldLength, '#');

        if($s === false) {
            throw new Exception("Coudln't read standard header field in directory header.");
        }

        return $s;
    }
}

class DupArchiveMiniGlobHeader //extends HeaderBase
{
    public $originalSize;
    public $storedSize;
    public $md5;

    const MaxHeaderSize = 255;

    public static function readFromArchive($archiveHandle, $skipPayload)
    {
        $instance = new DupArchiveMiniGlobHeader();

        $headerString = stream_get_line($archiveHandle, self::MaxHeaderSize, '!');

        if($headerString === false) {
            throw new Exception("Coudln't read standard header field in glob header.");
        }

        $marker = substr($headerString, 0, 3);

        if ($marker != '?G#') {
            throw new Exception("Invalid glob header marker found {$marker}. {$headerString} location:".ftell($archiveHandle));
        }

        $headerString = substr($headerString, 3);

        list($instance->originalSize, $instance->storedSize, $instance->md5) = explode('#', $headerString);

        if ($skipPayload) {

            if(fseek($archiveHandle, $instance->storedSize, SEEK_CUR) === -1)
            {
                throw new Exception("Can't fseek when skipping glob at location:".ftell($archiveHandle));
            }
        }

        return $instance;
    }
}

class DupArchiveMiniHeader
{
    public $version;
    public $isCompressed;

    const MaxHeaderSize = 50;

    private function __construct()
    {
        // Prevent instantiation
    }

    public static function readFromArchive($archiveHandle)
    {
        $instance = new DupArchiveMiniHeader();

        $headerString = stream_get_line($archiveHandle, self::MaxHeaderSize, '!');

        if ($headerString === false) {
            throw new Exception("Couldn't read archive header!");
        }

        $marker = substr($headerString, 0, 3);

        if ($marker != '?A#') {
            throw new Exception("Invalid archive header marker found {$marker}");
        }

        $headerString = substr($headerString, 3);

        list($instance->version, $isCompressedString) = explode('#', $headerString);

        $instance->version = (int) $instance->version;

        $instance->isCompressed = (($isCompressedString == 'true') ? true : false);

        return $instance;
    }
}

class DupArchiveMiniWriteInfo
{
    public $archiveHandle       = null;
    public $currentFileHeader   = null;
    public $destDirectory       = null;
    public $directoryWriteCount = 0;
    public $fileWriteCount      = 0;
    public $isCompressed        = false;
    public $enableWrite         = false;

    public function getCurrentDestFilePath()
    {
        if($this->destDirectory != null)
        {
            return "{$this->destDirectory}/{$this->currentFileHeader->relativePath}";
        }
        else
        {
            return null;
        }
    }

}

class DupArchiveMiniExpander
{

    public static $loggingFunction     = null;

    public static function init($loggingFunction)
    {
        self::$loggingFunction = $loggingFunction;
    }

    public static function log($s, $flush=false)
    {
        if(self::$loggingFunction != null) {
            call_user_func(self::$loggingFunction, "MINI EXPAND:$s", $flush);
        }
    }

    public static function expandDirectory($archivePath, $relativePath, $destPath)
    {
        self::expandItems($archivePath, $relativePath, $destPath);
    }

    private static function expandItems($archivePath, $inclusionFilter, $destDirectory, $ignoreErrors = false)
    {
        $archiveHandle = fopen($archivePath, 'r');

        if ($archiveHandle === false) {
            throw new Exception("Can’t open archive at $archivePath!");
        }

        $archiveHeader = DupArchiveMiniHeader::readFromArchive($archiveHandle);

        $writeInfo = new DupArchiveMiniWriteInfo();

        $writeInfo->destDirectory = $destDirectory;
        $writeInfo->isCompressed  = $archiveHeader->isCompressed;

        $moreToRead = true;

        while ($moreToRead) {

            if ($writeInfo->currentFileHeader != null) {

                try {
                    if (self::passesInclusionFilter($inclusionFilter, $writeInfo->currentFileHeader->relativePath)) {

                        self::writeToFile($archiveHandle, $writeInfo);

                        $writeInfo->fileWriteCount++;
                    }
                    else if($writeInfo->currentFileHeader->fileSize > 0) {
                      //  self::log("skipping {$writeInfo->currentFileHeader->relativePath} since it doesn’t match the filter");

                        // Skip the contents since the it isn't a match
                        $dataSize = 0;

                        do {
                            $globHeader = DupArchiveMiniGlobHeader::readFromArchive($archiveHandle, true);

                            $dataSize += $globHeader->originalSize;

                            $moreGlobs = ($dataSize < $writeInfo->currentFileHeader->fileSize);
                        } while ($moreGlobs);
                    }

                    $writeInfo->currentFileHeader = null;

                    // Expand state taken care of within the write to file to ensure consistency
                } catch (Exception $ex) {

                    if (!$ignoreErrors) {
                        throw $ex;
                    }
                }
            } else {

                $headerType = self::getNextHeaderType($archiveHandle);

                switch ($headerType) {
                    case DupArchiveMiniItemHeaderType::File:

                        $writeInfo->currentFileHeader = DupArchiveMiniFileHeader::readFromArchive($archiveHandle, $inclusionFilter);

                        break;

                    case DupArchiveMiniItemHeaderType::Directory:

                        $directoryHeader = DupArchiveMiniDirectoryHeader::readFromArchive($archiveHandle, true);

                     //   self::log("considering $inclusionFilter and {$directoryHeader->relativePath}");
                        if (self::passesInclusionFilter($inclusionFilter, $directoryHeader->relativePath)) {

                        //    self::log("passed");
                            $directory = "{$writeInfo->destDirectory}/{$directoryHeader->relativePath}";

                          //  $mode = $directoryHeader->permissions;
                            
                            // rodo handle this more elegantly @mkdir($directory, $directoryHeader->permissions, true);
                            @mkdir($directory, 0755, true);


                            $writeInfo->directoryWriteCount++;
                        }
                        else {
                     //       self::log("didnt pass");
                        }


                        break;

                    case DupArchiveMiniItemHeaderType::None:
                        $moreToRead = false;
                }
            }
        }

        fclose($archiveHandle);
    }

    private static function getNextHeaderType($archiveHandle)
    {
        $retVal = DupArchiveMiniItemHeaderType::None;
        $marker = fgets($archiveHandle, 4);

        if (feof($archiveHandle) === false) {
            switch ($marker) {
                case '?D#':
                    $retVal = DupArchiveMiniItemHeaderType::Directory;
                    break;

                case '?F#':
                    $retVal = DupArchiveMiniItemHeaderType::File;
                    break;

                case '?G#':
                    $retVal = DupArchiveMiniItemHeaderType::Glob;
                    break;

                default:
                    throw new Exception("Invalid header marker {$marker}. Location:".ftell($archiveHandle));
            }
        }

        return $retVal;
    }

    private static function writeToFile($archiveHandle, $writeInfo)
    {
		$destFilePath = $writeInfo->getCurrentDestFilePath();

		if($writeInfo->currentFileHeader->fileSize > 0)
		{
			/* @var $writeInfo DupArchiveMiniWriteInfo */
			$parentDir = dirname($destFilePath);

			if (!file_exists($parentDir)) {

				$r = @mkdir($parentDir, 0755, true);

				if(!$r)
				{
					throw new Exception("Couldn't create {$parentDir}");
				}
			}

			$destFileHandle = fopen($destFilePath, 'w+');

			if ($destFileHandle === false) {
				throw new Exception("Couldn't open {$destFilePath} for writing.");
			}

			do {

				self::appendGlobToFile($archiveHandle, $destFileHandle, $writeInfo);

				$currentFileOffset = ftell($destFileHandle);

				$moreGlobstoProcess = $currentFileOffset < $writeInfo->currentFileHeader->fileSize;
			} while ($moreGlobstoProcess);

			fclose($destFileHandle);

            @chmod($destFilePath, 0644);

			self::validateExpandedFile($writeInfo);
		} else {
			if(touch($destFilePath) === false) {
				throw new Exception("Couldn't create $destFilePath");
			}
            @chmod($destFilePath, 0644);
		}
    }

    private static function validateExpandedFile($writeInfo)
    {
        /* @var $writeInfo DupArchiveMiniWriteInfo */

        if ($writeInfo->currentFileHeader->md5 !== '00000000000000000000000000000000') {
            $md5 = md5_file($writeInfo->getCurrentDestFilePath());

            if ($md5 !== $writeInfo->currentFileHeader->md5) {

                throw new Exception("MD5 validation fails for {$writeInfo->getCurrentDestFilePath()}");
            }
        }
    }

    // Assumption is that archive handle points to a glob header on this call
    private static function appendGlobToFile($archiveHandle, $destFileHandle, $writeInfo)
    {
        /* @var $writeInfo DupArchiveMiniWriteInfo */
        $globHeader = DupArchiveMiniGlobHeader::readFromArchive($archiveHandle, false);

        $globContents = fread($archiveHandle, $globHeader->storedSize);

        if ($globContents === false) {

            throw new Exception("Error reading glob from {$writeInfo->getDestFilePath()}");
        }

        if ($writeInfo->isCompressed) {
            $globContents = gzinflate($globContents);
        }

        if (fwrite($destFileHandle, $globContents) === false) {
            throw new Exception("Error writing data glob to {$destFileHandle}");
        }
    }

    private static function passesInclusionFilter($filter, $candidate)
    {
        return (substr($candidate, 0, strlen($filter)) == $filter);
    }
}
?>